export default function request(url, requestProps) {
    return fetch(url, requestProps).then(results => {return results.json()});
}
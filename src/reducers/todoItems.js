import request from '../utils/request';

const initialState = {
    mainInputVal: '',
    globalLoaderActive: false,
    todos: [
        // {
        //     id: 0,
        //     text: 'lol',
        //     marked: false,
        //     onLoad: false,
        //     inEdit: false,
        //     editValue: ''
        // }
    ]
}

export default function todoItems(state = initialState, action) {
    let url,
        formData,
        requestProps,
        before,
        success,
        newState

    switch(action.type) {
        case 'MAIN_INPUT_CHANGE':
            return Object.assign({}, state, {
                mainInputVal: action.payload
            })

            break;

        case 'TODO_ADDBLOCK_BEFORE':
            return Object.assign({}, state, {
                globalLoaderActive: true
            })

            break;

        case 'TODO_ADDBLOCK_SUCCESS':
            newState = {};

            if(action.payload.responseData.status == 200) {
                Object.assign(newState, state, {
                    globalLoaderActive: false,
                    todos: state.todos,
                    mainInputVal: ''
                })

                newState.todos = [...newState.todos, {
                    id: action.payload.todo.id,
                    text: action.payload.todo.text,
                    marked: false
                }]

                return newState

            } else {
                alert('Error')
                return Object.assign({}, state, {
                    globalLoaderActive: false
                })
            }

        case 'TODOS_REFRESH_BEFORE':
            return Object.assign({}, state, {
                globalLoaderActive: true
            })

            break;

        case 'TODOS_REFRESH_SUCCESS':
            if(action.payload.status == 200) {
                if(action.payload.data == null) {
                    return Object.assign({}, state, {
                        todos: [],
                        globalLoaderActive: false
                    })
                }
                return Object.assign({}, state, {
                    todos: action.payload.data,
                    globalLoaderActive: false
                })
            } else {
                alert('Error while loading data from server')
            }

            break;

        case 'TODO_MARK_BEFORE':
            newState = {}
            Object.assign(newState, state)
            console.log(action.payload)
            newState.todos[action.payload].onLoad = true;

            return newState

            break;

        case 'TODO_MARK_SUCCESS':
            if(action.payload.responseData.status == 200) {
                let newState = {}
                console.log('payload from mark success: ' + action.payload.index)
                console.log('current state: ' + state.todos)
                
                Object.assign(newState, state, {
                    todos: state.todos,
                    globalLoaderActive: false
                })

                newState.todos[action.payload.index].marked = !newState.todos[action.payload.index].marked;
                newState.todos[action.payload.index].onLoad = false;

                return newState;
            } else {
                alert('Error while loading data from server')
                return Object.assign({}, state, {
                    // todos: {...state.todos},
                    globalLoaderActive: false
                })
            }

            break;

        case 'TODO_DELETE_BEFORE':
            newState = {}
            Object.assign(newState, state)
            console.log(action.payload)
            newState.todos[action.payload].onLoad = true;

            return newState;

            break;

        case 'TODO_DELETE_SUCCESS':
            if(action.payload.responseData.status == 200) {
                newState = {}
                
                Object.assign(newState, state, {
                    todos: state.todos,
                    globalLoaderActive: false
                })

                if (action.payload.index == 0) {
                    newState.todos.shift();
                } else if (action.payload.index > 0) {
                    newState.todos.splice(action.payload.index, 1)
                }
    
                return newState;

            } else {
                alert('Error while loading data from server')
                return Object.assign({}, state, {
                    globalLoaderActive: false
                })
            }

            break;

        case 'TODO_EDIT':
            newState = {};

            Object.assign(newState, state);

            newState.todos[action.payload.index].inEdit = true
            newState.todos[action.payload.index].editValue = newState.todos[action.payload.index].text

            return newState

            break;

        case 'TODO_EDIT_VALUE_HANDLER':

            newState = {}

            Object.assign(newState, state);

            newState.todos[action.payload.index].editValue = action.payload.text

            return newState

            break;

        case 'TODO_EDIT_CLOSE':
            newState = {};

            Object.assign(newState, state);

            newState.todos[action.payload.index].inEdit = false

            return newState

            break;

        case 'TODO_EDIT_BEFORE':
            newState = {}

            Object.assign(newState, state);

            newState.todos[action.payload].onLoad = true

            return newState

            break;

        case 'TODO_EDIT_SUCCESS':
            if(action.payload.responseData.status == 200) {
                newState = {}

                Object.assign(newState, state);

                newState.todos[action.payload.index].inEdit = false
                newState.todos[action.payload.index].onLoad = false
                newState.todos[action.payload.index].text = newState.todos[action.payload.index].editValue

                return newState
            }
            alert('Error')
            return state
            
        default: 
            return state
    }
    return state;
}
import {combineReducers} from 'redux';
import todoItems from './todoItems';
import todoLogin from './todoLogin';

const globalReducer = combineReducers({
    todoItems: todoItems,
    todoLogin: todoLogin
})

export default globalReducer;
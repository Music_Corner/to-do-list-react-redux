import request from '../../utils/request';

export const editClick = (index, todo) => {
    const data = {
        index: index,
        todo: todo
    }

    return {
        type: 'TODO_EDIT',
        payload: data
    }
}

export const editValueHandler = (text, index) => {
    const data = {
        text: text,
        index: index
    }

    return {
        type: 'TODO_EDIT_VALUE_HANDLER',
        payload: data
    }
}

export const editCloseClick = (index, todo) => {
    const data = {
        index: index,
        todo: todo
    }

    return {
        type: 'TODO_EDIT_CLOSE',
        payload: data
    }
}

export const editSaveBefore = (index) => {
    return {
        type: 'TODO_EDIT_BEFORE',
        payload: index
    }
}

export const editSaveSuccess = (data, index) => {
    data = {
        responseData: data,
        index: index
    }

    return {
        type: 'TODO_EDIT_SUCCESS',
        payload: data
    }
}

export const editSaveRequest = (token, todo, index) => {
    return dispatch => {
        const url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/ajaxUpdateText.php';
        const formData = new FormData;
        formData.append('token', token);
        formData.append('dataindex', todo.id);
        formData.append('updatedText', todo.editValue)

        const requestProps = {
            method: 'POST',
            body: formData
        }

        dispatch(editSaveBefore(index));

        request(url, requestProps).then(data => {
            dispatch(editSaveSuccess(data, index));
        })
    }
}
import request from '../../utils/request';

export const deleteBefore = (index) => {
    return {
        type: 'TODO_DELETE_BEFORE',
        payload: index
    }
}

export const deleteSuccess = (data, index) => {
    data = {
        responseData: data,
        index: index
    }

    return {
        type: 'TODO_DELETE_SUCCESS',
        payload: data
    }
}

export const deleteRequest = (token, todo, index) => {
    return dispatch => {
        const url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/ajaxDelete.php';
        const formData = new FormData;
        formData.append('token', token);
        formData.append('dataindex', todo.id)

        const requestProps = {
            method: 'POST',
            body: formData
        }

        dispatch(deleteBefore(index))

        request(url ,requestProps).then(data => {
            dispatch(deleteSuccess(data, index));
        })
    }
}
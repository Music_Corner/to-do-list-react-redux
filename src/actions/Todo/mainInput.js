export const mainInputChange = (value) => {
    return {
        type: 'MAIN_INPUT_CHANGE',
        payload: value
    }
}
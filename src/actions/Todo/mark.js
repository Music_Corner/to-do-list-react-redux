import request from '../../utils/request';

export const markBefore = (index) => {
    return {
        type: 'TODO_MARK_BEFORE',
        payload: index
    }
}

export const markSuccess = (data, index) => {
    data = {
        responseData: data,
        index: index
    }

    return {
        type: 'TODO_MARK_SUCCESS',
        payload: data
    }
}

export const markRequest = (token, todo, index, url) => {
    return dispatch => {
        const formData = new FormData;
        formData.append('token', token);
        formData.append('dataindex', todo.id)

        const requestProps = {
            method: 'POST',
            body: formData
        }

        dispatch(markBefore(index));

        request(url, requestProps).then(data => {
            dispatch(markSuccess(data, index));
        })
    }
}
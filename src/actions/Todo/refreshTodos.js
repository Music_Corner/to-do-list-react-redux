import request from '../../utils/request';

export const refreshBefore = () => {
    return {
        type: 'TODOS_REFRESH_BEFORE',
        payload: ''
    }
}

export const refreshSuccess = (data) => {
    console.log(data)
    return {
        type: 'TODOS_REFRESH_SUCCESS',
        payload: data
    }
}

export const refreshRequest = (token) => {
    return dispatch => {
        const url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/getData.php';
        const formData = new FormData;
        formData.append('token', token);

        const requestProps = {
            method: 'POST',
            body: formData
        }

        dispatch(refreshBefore());

        request(url, requestProps).then(data => {
            dispatch(refreshSuccess(data));
        })
    }
}

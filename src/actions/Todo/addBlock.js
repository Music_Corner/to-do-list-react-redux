import request from "../../utils/request";

export const addBlockBefore = () => {
    return {
        type: 'TODO_ADDBLOCK_BEFORE',
        payload: ''
    }
}

export const addBlockSuccess = (data, text, id) => {
    data = {
        responseData: data,
        todo: {
            id: id,
            text: text
        }
    }

    return {
        type: 'TODO_ADDBLOCK_SUCCESS',
        payload: data
    }
}

export const addBlockRequest = (token, text, id) => {
    return dispatch => {
        const url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/ajax.php';
        const formData = new FormData;
        formData.append('text', text)
        formData.append('token', token);
        formData.append('dataindex', id)

        const requestProps = {
            method: 'POST',
            body: formData
        }

        dispatch(addBlockBefore());

        request(url, requestProps).then(data => {
            dispatch(addBlockSuccess(data, text, id))
        })
    }
}
import React from 'react';
import styles from '../PasswordInput/PasswordInput.css';

function PasswordInput(props) {
    const {ChangePasswordHandler} = props;
    const {value} = props;
    return(
        <div className="PasswordInputBlock">
            <input onChange={ChangePasswordHandler} placeholder="Enter your password" type="password" value={value}></input>
        </div>
    )
}

export default PasswordInput
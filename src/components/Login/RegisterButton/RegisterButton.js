import React from 'react';
import styles from '../RegisterButton/RegisterButton.css';

function RegisterButton(props) {
    const {RegisterHandle} = props;
    const {validated} = props;

    if (validated == true) {
        return(
            <button onClick={RegisterHandle} className="RegisterButton">Register</button>
        )
    } else if (validated == false) {
        return(
            <button onClick={RegisterHandle}
            style={{'display': 'none'}} className="LoginButton">Register</button>
        )
    }
}

export default RegisterButton

import React from 'react';
import styles from '../LoginButton/LoginButton.css';

function LoginButton(props) {
    const {LoginHandle} = props;
    const {validated} = props;

    if (validated == true) {
        return(
            <button onClick={LoginHandle} className="LoginButton">Login</button>
        )
    } else if (validated == false) {
        return(
            <button onClick={LoginHandle}
            style={{'display': 'none'}} className="LoginButton">Login</button>
        )
    }
}

export default LoginButton
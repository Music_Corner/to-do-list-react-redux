import styles from '../Container/Container.css';
import React from 'react';
import {Component} from 'react';
import TitleBlock from '../TitleBlock/TitleBlock';
import LoginInput from '../LoginInput/LoginInput';
import PasswordInput from '../PasswordInput/PasswordInput';
import LoginButton from '../LoginButton/LoginButton';
import RegisterButton from '../RegisterButton/RegisterButton';
import ValidationPopup from '../ValidationPopup/ValidationPopup';
import Preloader from '../../Preloader/Preloader';
import NativeAppLink from '../../NativeAppLink/NativeAppLink'

class Container extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: false,
            requestUrl: 'https://app-1544250608.000webhostapp.com/todoAPI',
            form: {
                login: '',
                password: ''
            },
            preloaderActive: false,
            validated: true
        }

        this.LoginClick = this.LoginClick.bind(this);
        this.RegisterClick = this.RegisterClick.bind(this);
        this.LoginInputHandler = this.LoginInputHandler.bind(this);
        this.PasswordInputHandler = this.PasswordInputHandler.bind(this);

    }

    request(url, requestProps, success) {
        this.setState({preloaderActive: true})
        setTimeout(() => {
            fetch(url, requestProps).then(results => { return results.json() }).then(data => {
                console.log('token from response login: ' + data.token)
                this.setState({preloaderActive: false})
                if (data.authStatus == true) {
                    success(data)
                } else {
                    this.props.Auth(false)
                    alert('Invalid login or password');
                }
            }).catch((error) => {
                this.setState({preloaderActive: false})
                console.log('Error: ' + error)
                alert('Network Error')
               
                this.props.Auth(false)
            });
        }, 100)
    }

    LoginClick() {
        const url = this.state.requestUrl + '/application/php/ajax/ajaxLogin.php';
        let formData = new FormData;
        formData.append('login', this.state.form.login);
        formData.append('password', this.state.form.password);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            this.props.Auth(data.authStatus, data.token)
        }
        
        this.request(url, requestProps, success)
    }

    RegisterClick() {
        const url = this.state.requestUrl + '/application/php/ajax/ajaxRegister.php';
        let formData = new FormData;
        formData.append('login', this.state.form.login);
        formData.append('password', this.state.form.password);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            this.props.Auth(data.authStatus, data.token)
        }
        
        this.request(url, requestProps, success)
    }

    LoginInputHandler(event) {
        this.setState({
            form: {
                login: event.target.value,
                password: this.state.form.password
            }    
        });
    }

    PasswordInputHandler(event) {
        this.setState({
            form: {
                login: this.state.form.login,
                password: event.target.value
            }    
        });
    }

    ValidateInput() {
        const  pattern = /[^а-яА-Яa-zA-Z0-9_\-]/;
        if (pattern.test(this.state.login) || pattern.test(this.state.password)
        && this.state.login.length < 1 || this.state.password.length < 1) {
            this.setState({validated: false})
        } else {
            this.setState({validated: true});
        }
    }

    render() {
        let overflowStyle = {
            overflow: 'auto',
            filter: 'blur(0px)'
        }

        if(this.state.preloaderActive) {
           overflowStyle =  {
                overflow: 'hidden',
                filter: 'blur(10px)'
            }
        }

        return(
            <div>
                <Preloader
                active={this.state.preloaderActive}/>
                <div className='login-container' style={overflowStyle}>
                    <TitleBlock />
                    <LoginInput ChangeLoginHandler={this.LoginInputHandler}
                        value={this.state.login}/>
                    <PasswordInput ChangePasswordHandler={this.PasswordInputHandler}
                        value={this.state.password}/>
                    <RegisterButton RegisterHandle={this.RegisterClick}
                        validated={this.state.validated}/>
                    <LoginButton LoginHandle={this.LoginClick}
                        validated={this.state.validated}/>
                    {/* <ValidationPopup validated={this.state.validated} /> */}
                </div>
                <NativeAppLink style={overflowStyle}/>
            </div>
        )
    }
}

export default Container
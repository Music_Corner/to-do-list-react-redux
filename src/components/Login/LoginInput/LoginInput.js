import React from 'react';
import styles from '../LoginInput/LoginInput.css';

function LoginInput(props) {
    const {ChangeLoginHandler} = props;
    const {value} = props;
    return(
        <div className="LoginInputBlock">
            <input onChange={ChangeLoginHandler} placeholder="Enter your login" required autoComplete="false" value={value}></input>
        </div>
    )
}

export default LoginInput
import React from 'react';
import ToDo from './ToDo/ToDo';
import Login from './Login/Login';

class Router extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loggedIn: false,
            token: ''
        }

        this.Auth = this.Auth.bind(this)
    }

    componentWillMount() {
        const token = localStorage.getItem('token');
        if(token != null && token != '') {
            this.setState({
                loggedIn: true,
                token: token
            });
        } else {
            this.setState({
                loggedIn: false,
                token: ''
            }); 
        }
    }

    Auth(result, token) {
        if(result == false) {
            localStorage.setItem('token', '');
            this.setState({
                loggedIn: result,
                token: ''
            })
        } else {
            localStorage.setItem('token', token);
            this.setState({
                loggedIn: result,
                token: token
            })
        }
    }

    render() {
        if(this.state.loggedIn == true) {
            return(
                <div class="main">
                    <ToDo Auth={this.Auth} token={this.state.token}/>
                </div>
            )
        } else {
            return(
                <div class="main">
                    <Login Auth={this.Auth}/>
                </div>
            )
        }
    }
}

export default Router
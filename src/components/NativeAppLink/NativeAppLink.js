import React from 'react';
import {Component} from 'react';
import styles from '../NativeAppLink/NativeAppLink.css';

class NativeAppLink extends Component {
    constructor(props) {
        super(props);

        this.Redirect = this.Redirect.bind(this)
    }

    Redirect() {
        document.location = 'https://app-1544250608.000webhostapp.com/todoAPI/download.php'
    }

    render() {
        const {style} = this.props

        return(
            <div class="native-app-block" style={style} onClick={this.Redirect}>
                <span class="link-text">
                    Download native App for android:
                </span>
                <img class="android-img" src="assets/img/android-logo.png"></img>
            </div>
        )
    }
}

export default NativeAppLink;
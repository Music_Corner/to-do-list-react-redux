import styles from '../DeleteButton/DeleteButton.css'
import React from 'react';

function DeleteButton(props) {
    const {id} = props;
    const {index} = props;
    const {DeleteBlock} = props;
    return(
        <button onClick={function(){DeleteBlock(index, id)}} id={id} name="Delete_Button">Delete</button>
    )
}

export default DeleteButton
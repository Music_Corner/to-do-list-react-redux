import styles from '../EditButton/EditButton.css';
import React from 'react'

function EditButton(props) {
    const {EditClick} = props,
    {index} = props

    return(
        <button className='edit-button' onClick={() => {{EditClick(index)}}}>
            ✎
        </button>
    )
}

export default EditButton;
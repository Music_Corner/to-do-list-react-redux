import styles from '../EditableContentBlock/EditableContentBlock.css'
import React from 'react';
import SaveButton from '../SaveButton/SaveButton';
import CloseButton from '../CloseButton/CloseButton';
import SmallLoader from '../SmallLoader/SmallLoader';

function EditableContentBlock(props) {
    const {EditChangeHandler} = props,
    {EditSaveClick} = props,
    {EditCancelClick} = props,
    {newValue} = props,
    {index} = props,
    {id} = props,
    {onLoad} = props

    let blur = {
        filter: 'blur(0px)'
    }

    if(onLoad) {
        blur = {
            filter: 'blur(10px)'
        }
    }

    return(
        <div>
            <SmallLoader onLoad={onLoad}/> 
            <div className="editable-block" style={blur}>
                <input
                className="editable-input"
                onChange={EditChangeHandler}
                value={newValue}>
                </input>  

                <SaveButton
                id={id}
                EditSaveClick={EditSaveClick}
                index={index}/>

                <CloseButton
                EditCancelClick={EditCancelClick}
                id={id}
                index={index}/>
                
            </div>
        </div>
    )
}

export default EditableContentBlock
import styles from '../Container/Container.css';
import React, {Component} from 'react';
import InputWithAddButton from '../InputWithAddButton/InputWithAddButton';
import TitleBlock from '../TitleBlock/TitleBlock';
import ContentBlock from '../ContentBlock/ContentBlock';
import LogoutButton from '../LogoutButton/LogoutButton';
import RefreshButton from '../RefreshButton/RefreshButton';
import Preloader from '../../Preloader/Preloader';

class Container extends Component {
    constructor(props) {
        super(props)
       
        this.state = {
            myValue: '',
            newValue: '',
            items: [],
            RequestUrl: 'https://app-1544250608.000webhostapp.com/todoAPI',
            token: this.props.Token,
            preloaderActive: false,
            onLoad: false
        }

        this.AddBlock = this.AddBlock.bind(this);
        this.ChangeHandler = this.ChangeHandler.bind(this);
        this.EditChangeHandler = this.EditChangeHandler.bind(this);
        this.DeleteBlock = this.DeleteBlock.bind(this);
        this.MarkBlock = this.MarkBlock.bind(this);
        this.LogoutClick = this.LogoutClick.bind(this);
        this.EditClick = this.EditClick.bind(this);
        this.RefreshClick = this.RefreshClick.bind(this);
        this.EditSaveClick = this.EditSaveClick.bind(this);
        this.EditCancelClick = this.EditCancelClick.bind(this);
    }

    // updateData() {
    //     this.setState({
    //         items: [...this.state.items, ...data[0].text],
    //         marked: [...this.state.marked, ...data[0].marked]
    //     });
    //     console.log(this.state)
    // }

    componentWillMount() {
        const url = this.state.RequestUrl + '/application/php/ajax/getData.php'
        let formData = new FormData;
        formData.append('token', this.props.token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            if (data.data != null) {
                this.setState({
                    items: data.data
                })
            } else {
                this.setState({
                    items: [...this.state.items]
                })
            }
        }

        const beforeSend = () => {
            console.log('preloader is turning on')
            this.setState({
                preloaderActive: true
            })
        }

        this.request(url, requestProps, success, beforeSend)

    }

    request(url, requestProps, success, beforeSend) {
        console.log(url)
        beforeSend()
            fetch(url, requestProps).then(results => {return results.json() }).then(data => {
                console.log('response: ' + data.authStatus);
                if (data.authStatus == true) {
                    success(data);
                    this.setState({
                        preloaderActive: false
                    })
                } else {
                    this.props.Auth(false)
                }
            }).catch((error) => {
                this.setState({
                    preloaderActive: false,
                })

                console.log('request props: ' + requestProps.body.token)
                console.log('Error: ' + error + ' Message: ' + error.message)
            });
    }

    AddBlock(event) {
        event.preventDefault();
        let id = 0;
        if (this.state.items.length > 0) {
            id = +this.state.items[this.state.items.length - 1].id + 1
        } else {
            id = +this.state.items.length
        }
        const url = this.state.RequestUrl + '/application/php/ajax/ajax.php'
        let formData = new FormData;
        formData.append('text', this.state.myValue);
        formData.append('dataindex', id);
        formData.append('token', this.props.token);

        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.setState({
                items: [...this.state.items, {
                    text: this.state.myValue,
                    id: id,
                    marked: false,
                    inEdit: false
                }],
                myValue: '',
            })
        }

        const beforeSend = () => {
            this.setState({
                preloaderActive: true
            })
        }

        this.request(url, requestProps, success, beforeSend)
    }

    DeleteBlock(index, idFromBlock) {
        console.log(index)
        const url = this.state.RequestUrl + '/application/php/ajax/ajaxDelete.php';
        let formData = new FormData;
        formData.append('dataindex', idFromBlock);
        formData.append('token', this.props.token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            if (index == 0) {
                this.state.items.shift();
            } else if (index > 0) {
                this.state.items.splice(index, 1)
            }

            this.setState({
                items: [...this.state.items],
            })
        }

        const beforeSend = () => {
            this.state.items[index].onLoad = true;
            this.setState({});
        }

        this.request(url, requestProps, success, beforeSend)
    }

    MarkBlock(index, idFromBlock) {
        let formData = new FormData;
        formData.append('dataindex', idFromBlock);
        formData.append('token', this.props.token);
        let url = '';
            if (this.state.items[index].marked == true) {
                url = this.state.RequestUrl + '/application/php/ajax/ajaxUnmark.php';
            } else {
                url = this.state.RequestUrl + '/application/php/ajax/ajaxMark.php';
            }
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.state.items[index].marked = !this.state.items[index].marked;
            console.log('success')
            this.state.items[index].onLoad = false
            this.setState({
                items: [...this.state.items],
            })
        }

        const beforeSend = () => {
            this.state.items[index].onLoad = true;
            this.setState({});
            console.log(this.state.items[index].onLoad)
        }

        this.request(url, requestProps, success, beforeSend)
    }

    LogoutClick() {
        const url = this.state.RequestUrl + '/application/php/auth/authReset.php';
        let formData = new FormData;
        formData.append('token', this.props.token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            //Useless)))
        }

        const beforeSend =() => {
            this.setState({
                preloaderActive: true
            })
        }

        this.request(url, requestProps, success, beforeSend)
    }

    RefreshClick() {
        const url = this.state.RequestUrl + '/application/php/ajax/getData.php'
        let formData = new FormData;
        formData.append('token', this.props.token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            if (data.data != null) {
                this.setState({
                    items: data.data
                })
            } else {
                this.setState({
                    items: [...this.state.items]
                })
            }
        }

        const beforeSend = () => {
            this.setState({
                preloaderActive: true
            })
        }

        this.request(url, requestProps, success, beforeSend)
    }

    EditClick(index) {
        console.log(index)
        this.state.items[index].inEdit = true
        this.setState({
            newValue: this.state.items[index].text
        })
    }

    EditChangeHandler(event) {
        console.log(event)
        this.setState({newValue: event.target.value});
    }

    EditSaveClick(index, idFromBlock) {
        const url = this.state.RequestUrl + '/application/php/ajax/ajaxUpdateText.php';
        let formData = new FormData;
        formData.append('token', this.props.token);
        formData.append('dataindex', idFromBlock);
        formData.append('updatedText', this.state.newValue);

        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.state.items[index].inEdit = false;
            this.state.items[index].text = this.state.newValue;
            this.state.items[index].onLoad = false;
            this.setState({
                items: [...this.state.items],
                newValue: ''
            })
        }

        const beforeSend = () => {
            this.state.items[index].onLoad = true;
            this.setState({});
        }

        this.request(url, requestProps, success, beforeSend);
    }

    EditCancelClick(index, idFromBlock) {
        this.state.items[index].inEdit = false;
        this.setState({
            items: [...this.state.items],
            newValue: ''
        })
    }

    ChangeHandler(event) {
        this.setState({myValue: event.target.value});
    }

    render() {
        console.log(this.state);
        const contentBlock = this.state.items.map((item, index) => {
            if (this.state.items[index].text != null) {
                return (
                    <ContentBlock
                    marked={this.state.items[index].marked}
                    DeleteBlock={this.DeleteBlock}
                    MarkBlock={this.MarkBlock}
                    index={index}
                    id={this.state.items[index].id}
                    text={this.state.items[index].text}
                    newValue={this.state.newValue}
                    EditChangeHandler={this.EditChangeHandler}
                    EditClick={this.EditClick}
                    inEdit={this.state.items[index].inEdit}
                    EditSaveClick={this.EditSaveClick}
                    EditCancelClick={this.EditCancelClick}
                    onLoad={this.state.items[index].onLoad} />
                );
            }
        });

        let overflowStyle = {
            overflow: 'auto',
            filter: 'blur(0px)'
        }

        if(this.state.preloaderActive) {
           overflowStyle =  {
                overflow: 'hidden',
                filter: 'blur(10px)'
            }
        }

        return(
            <div>
                <Preloader
                active={this.state.preloaderActive}/>
                <div className="todo-container" style={overflowStyle}>
                    <div className="row space-between">
                        <LogoutButton LogoutClick={this.LogoutClick}/>
                        <TitleBlock />
                        <RefreshButton RefreshClick={this.RefreshClick}/>
                    </div>
                    <InputWithAddButton
                    value={this.state.myValue}
                    AddBlock = {this.AddBlock}
                    ChangeHandler = {this.ChangeHandler}
                    />
                
                    {contentBlock}
                </div>
            </div>
        )
    }
}

 export default Container

//  const styles = StyleSheet.create({    
//     container: {
//         flex: 1,
//         backgroundColor: 'orange',
//         alignItems: 'center',
//         borderWidth: 10
//       }
// })
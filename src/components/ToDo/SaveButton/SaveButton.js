import styles from '../SaveButton/SaveButton.css';
import React from 'react'

function SaveButton(props) {
    const {EditSaveClick} = props,
    {id} = props,
    {index} = props

    return(
        <button className='save-button' onClick={() => {EditSaveClick(index, id)}}>
            ✓
        </button>
    )
}

export default SaveButton;
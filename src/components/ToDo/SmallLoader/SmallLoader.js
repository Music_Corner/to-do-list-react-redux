import React from 'react';
import styles from '../SmallLoader/SmallLoader.css';

function SmallLoader() {
    return(
        <div class="loader-block">
            <div className='small-loader'>
            </div>
        </div>
    ) 
}

export default SmallLoader
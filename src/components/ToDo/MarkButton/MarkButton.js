import styles from '../MarkButton/MarkButton.css'
import React from 'react';

function MarkButton(props) {
    const {id} = props;
    const {index} = props;
    const {MarkBlock} = props;
    const {marked} = props;
    let title = 'Mark';
    if (marked === true) {
        title = 'Unmark'
    }
    return(
        <button onClick={function(){MarkBlock(index, id)}} id={id} name="Mark_Button">{title}</button>
    )
}

export default MarkButton
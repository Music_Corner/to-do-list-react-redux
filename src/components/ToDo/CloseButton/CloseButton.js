import styles from '../CloseButton/CloseButton.css';
import React from 'react'

function CloseButton(props) {
    const {EditCancelClick} = props,
    {id} = props,
    {index} = props

    return(
        <button className='close-button' onClick={() => {EditCancelClick(index, id)}}>
            ✕
        </button>
    )
}

export default CloseButton;
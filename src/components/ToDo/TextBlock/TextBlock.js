import styles from '../TextBlock/TextBlock.css'
import React from 'react';
import EditButton from '../EditButton/EditButton';

function TextBlock(props) {
    const {text} = props,
    {marked} = props,
    {EditClick} = props,
    {index} = props

    if (marked === false) {
        return(
            <div className="block">
                <EditButton
                EditClick={EditClick}
                index={index}/>
                {text}
            </div>
        )
    } else if (marked === true) {
        return(
            <div style={{'text-decoration': 'line-through'}} class="block">
                <EditButton
                EditClick={EditClick}
                index={index}/>
                {text}
            </div>
        )
    }
}

export default TextBlock
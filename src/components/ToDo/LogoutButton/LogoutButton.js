import styles from '../LogoutButton/LogoutButton.css'
import React from 'react';

function LogoutButton(props) {
    const {LogoutClick} = props;
    return(
        <button onClick={LogoutClick} id="Logout">
            ⟵
        </button>
    )
}

export default LogoutButton
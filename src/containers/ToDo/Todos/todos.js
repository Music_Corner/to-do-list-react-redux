import React, {Component} from 'react';
import {connect} from 'react-redux';
import styles from './todos.css';

import SmallLoader from '../../../components/ToDo/SmallLoader/SmallLoader'
import {markRequest} from '../../../actions/Todo/mark';
import {deleteRequest} from '../../../actions/Todo/delete';
import {refreshRequest} from '../../../actions/Todo/refreshTodos';
import {editClick, editSaveRequest, editCloseClick, editValueHandler} from '../../../actions/Todo/edit'
import request from '../../../utils/request';

const token = '21687c2f6da042c3f4274e4d1aa76923132d125d';

class Todos extends Component {
    componentWillMount() {
        this.props.refresh(token);
    }
    
    showToDos() {
        console.log(this.props.todos)
        return this.props.todos.todos.map((singleTodo, index) => {
            let singleTodoStyle = {
                'filter': 'blur(0px)'
            }

            let singleTodoLoader;

            if(singleTodo.onLoad) {
                singleTodoStyle = {
                    'filter': 'blur(10px)'
                }

                singleTodoLoader = <SmallLoader />
            }
            console.log(singleTodo)
            let marked;
            let markButtonText = 'Mark'
            let url;

            if(singleTodo.marked == true) {
                marked = {
                    'text-decoration': 'line-through'
                };
                markButtonText = 'Unmark'
                url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/ajaxUnmark.php';
            } else {
                marked = {
                    'text-decoration': 'none'
                };
                url = 'https://app-1544250608.000webhostapp.com/todoAPI/application/php/ajax/ajaxMark.php';
            }

            if(singleTodo.inEdit) {
                return(
                    <div>
                        {singleTodoLoader}
                        <div className="editable-block" style={singleTodoStyle}>
                            <input
                            className="editable-input"
                            onChange={(event) => {this.props.editValueHandler(event, index)}}
                            value={singleTodo.editValue}>
                            </input>  

                            <button className='save-button' onClick={() => {this.props.editSaveClick(token, singleTodo, index)}}>
                                ✓
                            </button>

                            <button className='close-button' onClick={() => {this.props.editCloseClick(index, singleTodo)}}>
                                ✕
                            </button>
                        </div>
                    </div>
                )
            }

            return(
                <div>
                    {singleTodoLoader}
                    <div key={index} className="single-todo" id={singleTodo.id} style={singleTodoStyle}>
                        <div className="todos-text" style={marked}>
                        <button className="edit-button" onClick={() => {this.props.editClick(index, singleTodo)}}>
                            ✎
                        </button>
                            {singleTodo.text}
                        </div>
                        <div className="buttons-block">
                            <button
                            onClick={() => {this.props.mark(token, singleTodo, index, url)}}
                            className="mark-button">
                            {markButtonText}
                            </button>

                            <button
                            onClick={() => {this.props.delete(token, singleTodo, index)}}
                            className="delete-button">
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            );
        })
    }

    render() {
        return(
            this.showToDos()
        )
    }
}

function mapStateToProps (state) {
    return {
        todos: state.todoItems
    }
}

function mapDispatchToProps(dispatch) {
    return {
        refresh: (token) => {
            dispatch(refreshRequest(token));
        },

        mark: (token, singleTodo, index, url) => {
            dispatch(markRequest(token, singleTodo, index, url));
        },

        delete: (token, singleTodo, index) => {
            dispatch(deleteRequest(token, singleTodo, index))
        },

        editClick: (index, todo) => {
            dispatch(editClick(index, todo))
        },

        editValueHandler: (event, index) => {
            const text = event.target.value
            dispatch(editValueHandler(text, index))
        },

        editCloseClick: (index, todo) => {
            dispatch(editCloseClick(index, todo))
        },

        editSaveClick: (token, todo, index) => {
            dispatch(editSaveRequest(token, todo, index))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Todos);

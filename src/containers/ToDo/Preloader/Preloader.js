import React, {Component} from 'react';
import styles from'../Preloader/Preloader.css';
import {connect} from 'react-redux';

class Preloader extends Component {

    render() {
        const active = this.props.active
        if(active) {
            return(
                <div className="preloader-block">
                        <div className='preloader'>
                        </div>
                    </div> 
                )
        } else {
            return(
                <div className="preloader-block" style={{'display': 'none'}}>
                    <div className='preloader'>
                    </div>
                </div>
            )
        }
    }
    
    
}

function mapStateToProps (state) {
    return {
        active: state.todoItems.globalLoaderActive
    }
}

export default connect(mapStateToProps)(Preloader)
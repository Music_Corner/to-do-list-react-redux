import React, {Component} from 'react';
import { connect } from 'react-redux';

import styles from './Todo.css';
import LogoutButton from '../../components/ToDo/LogoutButton/LogoutButton';
import InputWithAddButton from './InputWithAddButton/InputWithAddButton';
import TitleBlock from '../../components/ToDo/TitleBlock/TitleBlock';
import RefreshButton from './RefreshButton/RefreshButton';
import Preloader from './Preloader/Preloader';
import Todos from './Todos/todos';


class ToDo extends Component {
    render() {
        let containerStyle = {
            'overflow': 'auto',
            'filter': 'blur(0px)'
        }

        if(this.props.loading) {
            containerStyle = {
                'overflow': 'hidden',
                'filter': 'blur(10px)'
            }
        }
        
        return(
            <div>
                <Preloader/>
                <div className="todo-container" style={containerStyle}>
                    <div className="row space-between">
                        <LogoutButton/>
                        <TitleBlock />
                        <RefreshButton/>
                    </div>
                    <InputWithAddButton />
                    <Todos />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        loading: state.todoItems.globalLoaderActive
    }
}

export default connect(mapStateToProps)(ToDo)
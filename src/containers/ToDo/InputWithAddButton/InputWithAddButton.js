import React, {Component} from 'react';
import {connect} from 'react-redux';

import {mainInputChange} from '../../../actions/Todo/mainInput';
import {addBlockRequest} from '../../../actions/Todo/addBlock';
import request from '../../../utils/request';
import styles from '../InputWithAddButton/InputWithAddButton.css';

const token = '21687c2f6da042c3f4274e4d1aa76923132d125d';

class InputWithAddButton extends Component {
    render() {
        return(
            <form method="POST" autoComplete="off">
                <div className="blocktext">
                    
                <input className="mainInput"
                    placeholder="Enter something"
                    type="text"
                    id="enteredText"
                    name="input-name"
                    value={this.props.inputVal}
                    onChange={(event) => {this.props.ChangeHandler(event)}}
                />
                <button onClick={(event) => {this.props.AddBlock(event, this.props.todos, this.props.inputVal)}} type="submit" id="Add">Post</button>
                </div>
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {
        todos: state.todoItems.todos,
        inputVal: state.todoItems.mainInputVal
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ChangeHandler: (event) => {
            const value = event.target.value
            dispatch(mainInputChange(value))
        },

        AddBlock: (event, todos, text) => {
            event.preventDefault()

            let id = 1;
            // console.log('id of last elem in todos array: ' + todos[todos.length - 1].id)
            if(todos.length > 0) {
                id = parseInt(todos[todos.length - 1].id) + 1
            }

            dispatch(addBlockRequest(token, text, id));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputWithAddButton)
import React, {Component} from 'react';
import { connect } from 'react-redux';

import { refreshRequest } from '../../../actions/Todo/refreshTodos';
import request from '../../../utils/request'
import styles from '../RefreshButton/RefreshButton.css'

const token = '21687c2f6da042c3f4274e4d1aa76923132d125d';

class RefreshButton extends Component {
    render() {
        return(
            <button onClick={() => {this.props.refresh(token)}} id="Refresh">
                ⟳
            </button>
        )
    }
}

function mapStateToProps(state) {
    return {
        todos: state.todoItems.todos
    }
}

function mapDispatchToProps(dispatch) {
    return {
        refresh: (token) => {
            dispatch(refreshRequest(token));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RefreshButton)
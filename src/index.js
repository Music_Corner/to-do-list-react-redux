import React from 'react';
import thunk from 'redux-thunk'
import {render} from 'react-dom';
import './style/style.css';
import ToDoList from './containers/ToDo/ToDo';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import globalReducer from './reducers/index';

const store = createStore(globalReducer, applyMiddleware(thunk));

render(
    <Provider store={store}>
        <ToDoList />
    </Provider>,
    document.getElementById('root')
);